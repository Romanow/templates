import static org.apache.commons.io.FileUtils.moveFileToDirectory

Map<String,String> properties = [:]

properties.artifactId = ask('Выберите artifactId [template]: ', 'template')
properties.groupId = ask('Выберите groupId [ru.romanow.' +  properties.artifactId + ']: ', 'ru.romanow.' + properties.artifactId)
properties.version = ask('Выберите версию [1.0.0-SNAPSHOT]: ', '1.0.0-SNAPSHOT')
properties.pkg = ask("Выберите package для класса [$properties.groupId]:", properties.groupId)

processTemplates 'gradle.properties', properties
processTemplates '*.gradle', properties

def source = new File(projectDir, "build.gradle")
def target = new File(projectDir, "build.gradle")
target.write(source.text.replaceAll('@', '\\$'))

String packageDir = properties.pkg.replaceAll(/\./, '/')
moveFileToDirectory(new File(projectDir, 'src/main/java/Application.java'), new File(projectDir, "src/main/java/$packageDir"), true)
processTemplates 'src/main/java/**/Application.java', properties